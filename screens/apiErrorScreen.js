import {View, Text, StyleSheet, Image} from 'react-native';
import React, {useContext} from 'react';
import {UsefulContext} from '../App';

export default function ApiErrorScreen() {
  const {statusCode, errorMessage} = useContext(UsefulContext);
  return (
    <View>
      <Image
        source={require('../image/ErrorGif.gif')}
        style={{height: 350, width: 350, alignSelf: 'center'}}
      />
      <Text style={styles.errorText}>Error {statusCode}</Text>
      <Text style={styles.errorText}>{errorMessage}</Text>
      <Text style={styles.firstNote}>Note : 1.Please Check your API link.</Text>
      <Text style={styles.secondNote}>
        2.Please Check your internet Connection.
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: 'red',
  },
  errorText: {
    fontSize: 30,
    color: 'red',
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  firstNote: {
    fontSize: 15,
    color: 'black',
    fontWeight: 'bold',
    marginTop: 180,
  },
  secondNote: {
    fontSize: 15,
    color: 'black',
    fontWeight: 'bold',
    marginLeft: 45,
  },
});
