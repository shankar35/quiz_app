import {View, Text, TouchableOpacity, StyleSheet, FlatList} from 'react-native';
import React, {useContext, useEffect, useState} from 'react';
import {UsefulContext} from '../App';

export default function PreviousScreen({navigation}) {
  const {prevScoreArray} = useContext(UsefulContext);

  return (
    <View style={styles.main}>
      <View style={styles.header}>
        <Text style={styles.prevScoreText}>Previous Scores</Text>
      </View>
      <View style={styles.topMain}>
        <Text style={styles.userNameText}>User Name</Text>
        <Text style={styles.userScoreText}>Score</Text>
      </View>
      <View style={styles.flatListView}>
        <FlatList
          data={prevScoreArray}
          renderItem={({item}) => (
            <View>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('SpecificUserScoreScreen', {
                    key: item.finalScoreObject,
                    userName: item.userName,
                    userScore: item.userScore,
                  });
                }}>
                <Text style={styles.userText}>{item.userName}</Text>
                <Text style={styles.scoreText}>{item.userScore}</Text>
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  header: {
    flex: 0.1,
    backgroundColor: 'skyblue',
  },
  topMain: {
    flex: 0.1,
    borderWidth: 1,
    borderRadius: 20,
  },
  prevScoreText: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'black',
    alignSelf: 'center',
    marginTop: 10,
  },
  userNameText: {
    fontSize: 22,
    fontWeight: 'bold',
    color: 'black',
    marginLeft: 20,
    marginTop: 16,
  },
  userScoreText: {
    fontSize: 22,
    fontWeight: 'bold',
    color: 'black',
    marginLeft: 330,
    marginTop: -30,
  },
  flatListView: {
    flex: 0.8,
  },
  userText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'green',
    marginLeft: 25,
    marginTop: 20,
  },
  scoreText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'green',
    marginLeft: 350,
    marginTop: -20,
  },
});
