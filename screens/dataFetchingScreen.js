import {View, Text, StyleSheet} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useContext} from 'react';
import {UsefulContext} from '../App';
import {ActivityIndicator} from 'react-native';
import {StackActions} from '@react-navigation/native';

export default function DataFetchingScreen({navigation}) {
  const {setquestions, setErrorMessage, setStatusCode} =
    useContext(UsefulContext);

  const [loading, setLoading] = useState(true);
  const [isError, setISError] = useState(false);

  const api = 'https://6295e05175c34f1f3b23964b.mockapi.io/getData222';
  const dataFromAPI = () => {
    fetch(api)
      .then(responce => {
        if (!responce.ok) {
          setStatusCode(responce.status);
          throw Error('Not Found');
        } else {
          return responce.json();
        }
      })
      .then(data => {
        setquestions(data);
      })
      .catch(error => {
        setISError(true);
        setErrorMessage(error.message);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    dataFromAPI();
  }, []);

  return (
    <View>
      {loading ? (
        <View style={styles.loaderView}>
          <ActivityIndicator size="large" color="green" />
        </View>
      ) : isError ? (
        navigation.dispatch({...StackActions.replace('ApiErrorScreen')})
      ) : (
        navigation.dispatch({...StackActions.replace('DetailsScreen')})
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  loaderView: {
    justifyContent: 'center',
    marginTop: 300,
  },
});
