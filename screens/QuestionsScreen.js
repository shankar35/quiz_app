import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Alert,
  FlatList,
} from 'react-native';
import React, {useState, useContext} from 'react';
import {UsefulContext} from '../App';
import {StackActions} from '@react-navigation/native';

export default function QuestionsScreen({navigation}) {
  const {questions} = useContext(UsefulContext);

  const [number, setNumber] = useState(0);
  const [question, setQuestion] = useState(questions.questionsData[0]);
  const [changeNextToSubmit, setChangeNextToSubmit] = useState('Next');
  const [answerIdStore, setAnswerIdStore] = useState({});
  const [finalScoreObject, setFinalScoreObject] = useState({});

  const correctAnswersObjectHandler = cOption => {
    const correctAnswer = question.options.filter(
      itr => itr.option_id === cOption,
    );
    return correctAnswer;
  };

  const submitButtonHandler = () => {
    if (
      number === questions.questionsData.length - 1 &&
      number in answerIdStore
    ) {
      return true;
    }
  };

  const instantQuestionChangeHandler = questionNumber => {
    if (questionNumber in answerIdStore) {
      setNumber(questionNumber);
      setQuestion(questions.questionsData[questionNumber]);
    } else {
      Alert.alert('Warning..!', 'Question is not yet Answered');
    }
    if (questionNumber < questions.questionsData.length - 1) {
      setChangeNextToSubmit('Next');
    }
    if (
      questionNumber === questions.questionsData.length - 1 &&
      questionNumber in answerIdStore
    ) {
      setChangeNextToSubmit('Submit');
    }
  };

  const questionChangeHandler = () => {
    if (
      number < questions.questionsData.length - 1 &&
      number in answerIdStore
    ) {
      setNumber(number + 1);
    } else {
      Alert.alert('Warning..!', 'You must select any one of the option');
    }

    setQuestion(questions.questionsData[number + 1]);

    if (number === questions.questionsData.length - 2) {
      setChangeNextToSubmit('Submit');
    }
  };

  const prevQuestionChangeHandler = () => {
    if (number + 1 >= 1) {
      setChangeNextToSubmit('Next');
      setNumber(number - 1);
    }
    setQuestion(questions.questionsData[number - 1]);
  };

  const answerIdStoreHandler = (optionId, selectedOption, correctOption2) => {
    var correctOption1 = correctAnswersObjectHandler(correctOption2);
    correctOption1[0].option;
    setAnswerIdStore(prevAnswer => {
      return {...prevAnswer, [number]: optionId};
    });

    setFinalScoreObject(PrevScoreObject => {
      return {
        ...PrevScoreObject,
        [number]: {
          shownQuestion: number + 1 + '.' + question.question,
          shownOption1: 'a)' + '.' + option1,
          shownOption2: 'b)' + '.' + option2,
          shownOption3: 'c)' + '.' + option3,
          shownOption4: 'd)' + '.' + option4,
          correctOption: correctOption1[0].option,
          choosenOption: selectedOption,
        },
      };
    });
  };

  const option1 = question.options[0].option;
  const option2 = question.options[1].option;
  const option3 = question.options[2].option;
  const option4 = question.options[3].option;

  const option0Id = question.options[0].option_id;
  const option1Id = question.options[1].option_id;
  const option2Id = question.options[2].option_id;
  const option3Id = question.options[3].option_id;

  const correctOption = question.correct_option;

  return (
    <View style={styles.main}>
      <View style={styles.headerbar}>
        <Text style={styles.header}>Question {number + 1}</Text>
      </View>
      <View style={styles.questionNumbersView}>
        <FlatList
          horizontal
          data={questions.questionsData}
          renderItem={({index}) => (
            <TouchableOpacity
              onPress={() => {
                instantQuestionChangeHandler(index);
              }}>
              <View style={styles.questionNumberOuterView}>
                <Text
                  style={{
                    ...styles.questionNumberInsideFlatlist,
                    ...{
                      backgroundColor:
                        index in answerIdStore ? 'green' : 'white',
                    },
                  }}>
                  {'    '}
                  {index + 1}
                  {'    '}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
      <View style={styles.downAfterFlatlistView}>
        <Text style={styles.headerQuestion}>{question.question}</Text>
        <View>
          <TouchableOpacity
            onPress={() =>
              answerIdStoreHandler(option0Id, option1, correctOption)
            }>
            <View style={styles.radioButtonOff}>
              <View
                style={{
                  ...styles.radioButtonOn,
                  ...{
                    backgroundColor:
                      answerIdStore[number] === option0Id ? 'black' : 'white',
                  },
                }}
              />
            </View>
            <View style={styles.optionView}>
              <Text style={styles.options}>{option1}</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            onPress={() =>
              answerIdStoreHandler(option1Id, option2, correctOption)
            }>
            <View style={styles.radioButtonOff}>
              <View
                style={{
                  ...styles.radioButtonOn,
                  ...{
                    backgroundColor:
                      answerIdStore[number] === option1Id ? 'black' : 'white',
                  },
                }}></View>
            </View>
            <View style={styles.optionView}>
              <Text style={styles.options}>{option2}</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            onPress={() =>
              answerIdStoreHandler(option2Id, option3, correctOption)
            }>
            <View style={styles.radioButtonOff}>
              <View
                style={{
                  ...styles.radioButtonOn,
                  ...{
                    backgroundColor:
                      answerIdStore[number] === option2Id ? 'black' : 'white',
                  },
                }}></View>
            </View>
            <View style={styles.optionView}>
              <Text style={styles.options}>{option3}</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            onPress={() =>
              answerIdStoreHandler(option3Id, option4, correctOption)
            }>
            <View style={styles.radioButtonOff}>
              <View
                style={{
                  ...styles.radioButtonOn,
                  ...{
                    backgroundColor:
                      answerIdStore[number] === option3Id ? 'black' : 'white',
                  },
                }}></View>
            </View>
            <View style={styles.optionView}>
              <Text style={styles.options}>{option4}</Text>
            </View>
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          onPress={() => {
            number > 0 ? prevQuestionChangeHandler() : '';
          }}>
          <View style={styles.prevButtonView}>
            <Text style={styles.prevButton}>Prev</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            changeNextToSubmit === 'Submit'
              ? submitButtonHandler()
                ? navigation.dispatch({
                    ...StackActions.replace('SubmitScreen', {finalScoreObject}),
                  })
                : Alert.alert(
                    'warning',
                    'You must select any one of the option',
                  )
              : questionChangeHandler();
          }}>
          <View style={styles.nextButtonView}>
            <Text style={styles.nextButton}>{changeNextToSubmit}</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  questionNumbersView: {
    flex: 0.1,
  },
  downAfterFlatlistView: {
    flex: 0.8,
  },
  questionNumberInsideFlatlist: {
    marginTop: 20,
    fontSize: 25,
    borderRadius: 20,
    borderWidth: 1,
    height: 40,
    fontWeight: 'bold',
    color: 'black',
  },
  nextButton: {
    borderWidth: 2,
    marginLeft: 300,
    marginRight: 10,
    borderRadius: 20,
    fontSize: 20,
    color: 'black',
    alignSelf: 'center',
    padding: 10,
  },
  prevButton: {
    borderWidth: 2,
    marginLeft: 10,
    marginRight: 300,
    borderRadius: 20,
    fontSize: 20,
    color: 'black',
    alignSelf: 'center',
    padding: 10,
  },
  nextButtonView: {
    marginTop: -45,
  },
  prevButtonView: {
    marginTop: 100,
  },
  headerbar: {
    flex: 0.1,
    backgroundColor: 'skyblue',
  },
  header: {
    alignSelf: 'center',
    fontSize: 28,
    fontWeight: 'bold',
    color: 'black',
    marginTop: 12,
  },
  headerQuestion: {
    padding: 15,
    fontSize: 30,
    fontWeight: 'bold',
    color: 'black',
  },
  radioButtonOff: {
    width: 27,
    height: 27,
    borderRadius: 27,
    borderWidth: 1,
    marginTop: 10,
    marginLeft: 10,
  },
  options: {
    marginLeft: 50,
    fontSize: 23,
    fontWeight: 'bold',
    color: 'black',
  },
  optionView: {
    marginTop: -29,
  },
  radioButtonOn: {
    width: 17,
    height: 17,
    borderWidth: 1,
    borderRadius: 17,
    borderColor: 'white',
    alignSelf: 'center',
    marginTop: 3.5,
  },
});
