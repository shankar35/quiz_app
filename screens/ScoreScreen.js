import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import React, {useContext, useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {UsefulContext} from '../App';

export default function ScoreScreen({navigation, route}) {
  const {prevScoreArray, setPrevScoreArray, inputName} =
    useContext(UsefulContext);

  finalScoreObject = route.params.finalScoreObject;

  useEffect(() => {
    storeData(prevScoreArray);
  }, []);

  const storeData = async prevScoreArray => {
    try {
      prevScoreArray.push({
        userName: inputName,
        userScore: score,
        finalScoreObject: finalScoreObject,
      });
      setPrevScoreArray(prevScoreArray);
      const jsonValue = JSON.stringify(prevScoreArray);
      AsyncStorage.setItem('userDetails', jsonValue);
    } catch (e) {
      console.log(e);
    }
  };

  var score = 0;
  var finalList = [];

  for (var object in finalScoreObject) {
    if (
      finalScoreObject[object].choosenOption ===
      finalScoreObject[object].correctOption
    ) {
      score += 1;
    }
    finalList.push(finalScoreObject[object]);
  }

  return (
    <View style={styles.main}>
      <View style={styles.topView}>
        <Text style={styles.headerText}>
          Congrats {inputName}...! Your Quiz is Completed
        </Text>
        <Text style={styles.scoreHeader}>Your Score is {score}</Text>
      </View>
      <View style={styles.flatListView}>
        <FlatList
          data={finalList}
          renderItem={({item}) => (
            <View>
              <Text
                style={{
                  ...styles.headerQuestion,
                  ...{
                    color:
                      item.choosenOption === item.correctOption
                        ? 'green'
                        : 'red',
                  },
                }}>
                {item.shownQuestion}
              </Text>
              <Text
                style={{
                  ...styles.showQuestions,
                  ...{
                    color:
                      item.choosenOption === item.correctOption
                        ? 'green'
                        : 'red',
                  },
                }}>
                {item.shownOption1}
              </Text>
              <Text
                style={{
                  ...styles.showQuestions,
                  ...{
                    color:
                      item.choosenOption === item.correctOption
                        ? 'green'
                        : 'red',
                  },
                }}>
                {item.shownOption2}
              </Text>
              <Text
                style={{
                  ...styles.showQuestions,
                  ...{
                    color:
                      item.choosenOption === item.correctOption
                        ? 'green'
                        : 'red',
                  },
                }}>
                {item.shownOption3}
              </Text>
              <Text
                style={{
                  ...styles.showQuestions,
                  ...{
                    color:
                      item.choosenOption === item.correctOption
                        ? 'green'
                        : 'red',
                  },
                }}>
                {item.shownOption4}
              </Text>
              <Text
                style={{
                  ...styles.choosenOptionText,
                  ...{
                    color:
                      item.choosenOption === item.correctOption
                        ? 'green'
                        : 'red',
                  },
                }}>
                Choosen option : {item.choosenOption}
              </Text>
              <Text style={styles.correctOptionText}>
                Correct option : {item.correctOption}
              </Text>
            </View>
          )}
        />
      </View>
      <TouchableOpacity onPress={() => navigation.navigate('Home')}>
        <View style={styles.button1}>
          <Text style={styles.closeButton}>Close</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: 'snow',
  },
  headerText: {
    textAlign: 'center',
    color: 'green',
    fontSize: 20,
    fontWeight: 'bold',
  },
  scoreHeader: {
    textAlign: 'center',
    color: 'green',
    fontSize: 20,
    marginTop: 5,
    fontWeight: 'bold',
  },
  headerQuestion: {
    marginTop: 10,
    fontSize: 21,
    color: 'black',
    fontWeight: 'bold',
    marginLeft: 10,
  },
  showQuestions: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',
    marginLeft: 25,
  },
  topView: {
    flex: 0.13,
    borderWidth: 2,
    borderRadius: 10,
  },
  flatListView: {
    flex: 0.8,
    borderWidth: 2,
    borderRadius: 10,
    marginTop: 3,
  },
  choosenOptionText: {
    fontSize: 18,
    marginTop: 5,
    marginLeft: 25,
  },
  correctOptionText: {
    fontSize: 18,
    color: 'green',
    marginLeft: 25,
  },
  button1: {
    flex: 0.07,
  },
  closeButton: {
    fontSize: 18,
    fontWeight: 'bold',
    borderWidth: 1,
    width: 80,
    height: 35,
    marginLeft: 300,
    marginBottom: -10,
    alignSelf: 'center',
    textAlign: 'center',
    borderRadius: 20,
    marginTop: 5,
    color: 'black',
  },
});
