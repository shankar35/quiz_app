import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import React from 'react';

export default function SpecificUserScoreScreen({navigation, route}) {
  var score = 0;
  var finalList = [];

  var ScoreObject = route.params.key;
  var inputName = route.params.userName;
  var score = route.params.userScore;

  for (var object in ScoreObject) {
    finalList.push(ScoreObject[object]);
  }

  return (
    <View style={styles.main}>
      <View style={styles.topView}>
        <Text style={styles.headerText}>Name : {inputName}</Text>
        <Text style={styles.scoreHeader}>Score : {score}</Text>
      </View>
      <View style={styles.flatListView}>
        <FlatList
          data={finalList}
          renderItem={({item}) => (
            <View
              style={{
                ...styles.questionsView,
                ...{
                  backgroundColor:
                    item.choosenOption === item.correctOption
                      ? 'white'
                      : 'white',
                },
              }}>
              <Text
                style={{
                  ...styles.headerQuestion,
                  ...{
                    color:
                      item.choosenOption === item.correctOption
                        ? 'green'
                        : 'red',
                  },
                }}>
                {item.shownQuestion}
              </Text>
              <Text
                style={{
                  ...styles.showQuestions,
                  ...{
                    color:
                      item.choosenOption === item.correctOption
                        ? 'green'
                        : 'red',
                  },
                }}>
                {item.shownOption1}
              </Text>
              <Text
                style={{
                  ...styles.showQuestions,
                  ...{
                    color:
                      item.choosenOption === item.correctOption
                        ? 'green'
                        : 'red',
                  },
                }}>
                {item.shownOption2}
              </Text>
              <Text
                style={{
                  ...styles.showQuestions,
                  ...{
                    color:
                      item.choosenOption === item.correctOption
                        ? 'green'
                        : 'red',
                  },
                }}>
                {item.shownOption3}
              </Text>
              <Text
                style={{
                  ...styles.showQuestions,
                  ...{
                    color:
                      item.choosenOption === item.correctOption
                        ? 'green'
                        : 'red',
                  },
                }}>
                {item.shownOption4}
              </Text>
              <Text
                style={{
                  ...styles.choosenOptionText,
                  ...{
                    color:
                      item.choosenOption === item.correctOption
                        ? 'green'
                        : 'red',
                  },
                }}>
                choosen option : {item.choosenOption}
              </Text>
              <Text style={styles.correctOptionText}>
                Correct option : {item.correctOption}
              </Text>
            </View>
          )}
        />
      </View>
      <TouchableOpacity onPress={() => navigation.navigate('PreviousScreen')}>
        <View style={styles.button1}>
          <Text style={styles.closeButton}>Close</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: 'snow',
  },
  headerText: {
    textAlign: 'center',
    color: 'green',
    fontSize: 20,
    fontWeight: 'bold',
  },
  scoreHeader: {
    textAlign: 'center',
    color: 'green',
    fontSize: 20,
    marginTop: 5,
    fontWeight: 'bold',
  },
  headerQuestion: {
    marginTop: 20,
    fontSize: 21,
    color: 'black',
    fontWeight: 'bold',
  },
  showQuestions: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',
  },
  questionsView: {
    marginBottom: 10,
  },
  topView: {
    flex: 0.11,
  },
  flatListView: {
    flex: 0.8,
  },
  choosenOptionText: {
    fontSize: 18,
    marginTop: 5,
  },
  correctOptionText: {
    fontSize: 18,
    color: 'green',
  },
  button1: {
    flex: 0.02,
  },
  closeButton: {
    fontSize: 18,
    fontWeight: 'bold',
    borderWidth: 1,
    width: 80,
    height: 35,
    marginLeft: 300,
    marginBottom: -20,
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    marginTop: 10,
    color: 'black',
  },
});
