import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import React, {useContext} from 'react';
import {UsefulContext} from '../App';
import {StackActions} from '@react-navigation/native';

export default function EnterDetailsScreen({navigation}) {
  const {inputName, setInputName, prevScoreArray} = useContext(UsefulContext);

  const inputNameHandler = nameText => {
    setInputName(nameText.trim());
  };

  const inputNameValidator = () => {
    if (inputName.length > 3 && inputName.length <= 10) {
      for (var itr = 0; itr < prevScoreArray.length; itr++) {
        if (prevScoreArray[itr].userName === inputName) {
          Alert.alert(
            'Warning..!',
            'This Name is already in use.Please Try with another name.',
          );
          return false;
        }
      }
      return true;
    } else {
      Alert.alert(
        'OOPs!',
        'Name Should be of more than 3 letters and less than 10 letters.',
      );
      return false;
    }
  };

  return (
    <View style={styles.mainView}>
      <View style={styles.insideMainView}>
        <View>
          <Text style={styles.headerName}>Enter Your Name</Text>
        </View>
        <View>
          <TextInput
            style={styles.textInput}
            onChangeText={inputText => inputNameHandler(inputText)}></TextInput>
        </View>
        <View style={styles.cancelButtonView}>
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <Text style={styles.cancelButton}>Cancel</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() => {
            inputNameValidator()
              ? navigation.dispatch({...StackActions.replace('Questions')})
              : navigation.navigate('DetailsScreen');
          }}>
          <View style={styles.proceedButtonView}>
            <Text style={styles.proceedButton}>Proceed</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'linen',
  },
  insideMainView: {
    width: 350,
    height: 200,
    alignSelf: 'center',
    borderRadius: 30,
    borderWidth: 2,
    backgroundColor: 'lightgray',
  },
  textInput: {
    borderWidth: 1,
    marginTop: 20,
    borderRadius: 10,
    width: 250,
    alignSelf: 'center',
  },
  headerName: {
    textAlign: 'center',
    marginTop: 10,
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',
  },
  cancelButtonView: {
    borderWidth: 1,
    borderRadius: 6,
    width: 70,
    height: 40,
    marginLeft: 20,
    marginTop: 35,
  },
  cancelButton: {
    alignSelf: 'center',
    marginTop: 7,
    fontSize: 15,
    fontWeight: 'bold',
    color: 'black',
  },
  proceedButtonView: {
    borderWidth: 1,
    borderRadius: 6,
    width: 70,
    height: 40,
    marginLeft: 260,
    marginTop: -38,
  },
  proceedButton: {
    alignSelf: 'center',
    marginTop: 7,
    fontSize: 15,
    fontWeight: 'bold',
    color: 'black',
  },
});
