import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import {UsefulContext} from '../App';
import {useContext} from 'react';

export default function HomeScreen({navigation}) {
  const {questions} = useContext(UsefulContext);
  return (
    <>
      <View style={styles.main}>
        <View>
          <Text style={styles.header}>Quiz</Text>
        </View>
        <View style={styles.imageView}>
          <Image source={require('../image/image.png')} style={styles.image} />
        </View>
        <View style={styles.buttonView}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('DataFetchingScreen');
            }}>
            <Text style={styles.button1}>Take test</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('PreviousScreen')}>
            <Text style={styles.button2}>Previous Score</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: 'paleturquoise',
  },
  header: {
    fontSize: 30,
    alignSelf: 'center',
    padding: 30,
    fontWeight: 'bold',
    color: 'black',
  },
  image: {
    width: 200,
    height: 200,
    marginTop: 100,
    borderRadius: 45,
  },
  imageView: {
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'space-around',
  },
  buttonView: {
    flex: 1,
    width: 200,
    alignSelf: 'center',
  },
  button1: {
    textAlign: 'center',
    padding: 10,
    borderWidth: 3,
    borderRadius: 30,
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
    marginTop: 40,
  },
  button2: {
    textAlign: 'center',
    padding: 10,
    borderWidth: 3,
    borderRadius: 30,
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
    marginTop: 20,
  },
});
