import React, {useState, useEffect} from 'react';
import HomeScreen from './screens/HomeScreen';
import QuestionsScreen from './screens/QuestionsScreen';
import ScoreScreen from './screens/ScoreScreen';
import EnterDetailsScreen from './screens/EnterDetailsScreen';
import PreviousScreen from './screens/PreviousScreen';
import SpecificUserScoreScreen from './screens/SpecificUserScoreScreen';
import ApiErrorScreen from './screens/apiErrorScreen';
import DataFetchingScreen from './screens/dataFetchingScreen';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createContext} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Stack = createNativeStackNavigator();
const UsefulContext = createContext();

const App = () => {
  const [inputName, setInputName] = useState('');
  const [prevScoreArray, setPrevScoreArray] = useState([]);
  const [questions, setquestions] = useState();
  const [errorMessage, setErrorMessage] = useState();
  const [statusCode, setStatusCode] = useState();

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    try {
      await AsyncStorage.getItem('userDetails').then(value => {
        if (value !== null) {
          const value1 = JSON.parse(value);
          setPrevScoreArray(value1);
        }
      });
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <UsefulContext.Provider
      value={{
        inputName,
        setInputName,
        prevScoreArray,
        setPrevScoreArray,
        questions,
        setquestions,
        errorMessage,
        setErrorMessage,
        statusCode,
        setStatusCode,
      }}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="DetailsScreen"
            component={EnterDetailsScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Questions"
            component={QuestionsScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="SubmitScreen"
            component={ScoreScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="PreviousScreen"
            component={PreviousScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="SpecificUserScoreScreen"
            component={SpecificUserScoreScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="DataFetchingScreen"
            component={DataFetchingScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="ApiErrorScreen"
            component={ApiErrorScreen}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </UsefulContext.Provider>
  );
};

export {UsefulContext};
export default App;
