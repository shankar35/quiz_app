import {createStackNavigator} from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import QuestionsScreen from '../screens/QuestionsScreen';
import HomeScreen from '../screens/HomeScreen';
const screens = {
  Home: {
    screen: HomeScreen,
  },
  QuestionsScreen: {
    screen: QuestionsScreen,
  },
};

const HomeStack = createAppContainer(screens);

export default createAppContainer(HomeStack);
